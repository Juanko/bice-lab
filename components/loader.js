import { LoaderContainer } from '../public/assets/css/loader';
import Loader from 'react-loader-spinner';

function LoaderComponent(){
    return (
        <LoaderContainer>
            <div className="overlay" />
            <div className="container">
                <img src="/assets/img/logo.png" /><br />
                <Loader type="BallTriangle" color="#005a96" height={80} width={80} />
            </div>
        </LoaderContainer>
    );
}

export default LoaderComponent;