import {
    CardContainer,
} from '../public/assets/css/card';
function Card(props){
    return (
        <CardContainer to={`/detail/${props.path_detail}`}>
            <h3 className="card-label">
                {props.title}
            </h3>
            <div>{props.value}</div>
        </CardContainer>
    );
}

export default Card;