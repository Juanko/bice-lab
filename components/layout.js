import Head from 'next/head'
import Header from './header';
import { NotificationContainer } from 'react-notifications';
import { Main } from '../public/assets/css/layout';

export default function Layout(props) {
  return (
    <div className="container">
      <Head>
        <title>Indicators</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      <Header />
      <Main>
        {props.children}
      </Main>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: Roboto, Helvetica Neue, sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
      <NotificationContainer />
    </div>
  )
}
