import {
  Link,
  Logo,
  LinkMenu,
  HeaderBar
} from '../public/assets/css/home';

function Header(){
    return (
        <HeaderBar>
            <Link to="/">
              <img src="/assets/img/logo.png" />
            </Link>
        </HeaderBar>
    );
}

export default Header;