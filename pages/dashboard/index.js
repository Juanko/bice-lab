import { Component } from 'react';
import Layout from '../../components/layout';
import Loader from '../../components/loader';
import Card from '../../components/card';
import {NotificationManager} from 'react-notifications';
import Api from '../../utils/api/indicators';
import {IndicatorsContainer} from '../../public/assets/css/dashboard';

class Home extends Component {
  constructor(props){
    super(props);
    
    this.state = {
      loader: true,
      indicators: []
    }
  }
  componentDidMount = async () => {
    const indicator = await Api.getIndicators();
    if(indicator.status === 200){
      var indicators = []
      Object.entries(indicator.data).forEach(([key, value]) => indicators.push(value)) 
      this.setState({indicators})
    }else{
      NotificationManager.error('No se pudo obtener los indicadores, intentalo en unos minutos', 'Error al Obtener los datos');
    }
    this.setState({loader: false})
  }
  render(){
    if(this.state.loader){
      return <Loader />
    }
    console.log(this.state.indicators)
    return (
      <Layout>
          <h1>Indicadores</h1>
          <IndicatorsContainer>
            {this.state.indicators.map((indicator) => <Card title={indicator.name} value={indicator.value} path_detail={indicator.key} />)}
          </IndicatorsContainer>
      </Layout>
    )
  }
}


export default Home;