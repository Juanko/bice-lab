import 'react-notifications/lib/notifications.css';
import React from 'react'
import { Router } from '@reach/router'

import Dashboard from './dashboard'
import Detail from './detail'

export default function () {
  return (
      <Router>
        <Dashboard path='/' />
        <Detail path='/detail/:indicator' />
        {/* <Cobre path='/cobre' />
        <Cobre path='/cobre/:date' /> */}
      </Router>
  )
}