import React from 'react'
import { Router } from 'next/router'

import Dashboard from './dashboard'
import Detail from './detail'

export default function () {
  return (
      <Router>
        <Dashboard path='/' />
        <Detail path='/detail/:indicator' />
        {/* <Cobre path='/cobre' />
        <Cobre path='/cobre/:date' /> */}
      </Router>
  )
}
