export default (req, res) => {
    const url = `${process.env.api_url}`;
    console.debug(url);
    res.statusCode = 200
    fetch(url, {
        method: 'GET',
        crossDomain: true,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        }
    })
    .then((response) => response.json())
    .then((responseJson) => res.json(responseJson))
    .catch((error) => {
        console.error(error);
        res.statusCode = 400
        res.json(error);
    });
}
