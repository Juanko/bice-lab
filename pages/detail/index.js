import React, {Component, useState, useMemo} from 'react';
import Layout from '../../components/layout';
// import { Chart } from 'react-charts'
import { Doughnut } from 'react-chartjs-2';
import Indicators from '../../utils/api/indicators';
import Loader from '../../components/loader';
import { NotificationManager } from 'react-notifications';
import { Table, Container } from '../../public/assets/css/detail';

class Detail extends Component {
    constructor(props){
        super(props);
        
        this.state = {
          loader: true,
          data: {}
        }
    }
  componentDidMount = async () => {
    const detail = await Indicators.getDetail(this.props.indicator);
    if(detail.status === 200){
      console.log("detail",detail);
      var values = []
      Object.entries(detail.data.values).forEach(([key, value]) => values.push({date: key, value: value})) 
      this.setState({
        data: values.slice(-10)
      })
    }else{
      NotificationManager.error('No se pudo obtener los indicadores, intentalo en unos minutos', 'Error al Obtener los datos');
    }
    this.setState({loader: false});
  }

  render(){
    if(this.state.loader){
      return(
        <Layout>
          <Loader />
        </Layout>
      )
    }
    return (
      <Layout>
          <Container>
          <h1>{this.props.indicator}</h1>
          <Table border={1}>
              <thead>
                  <th>Fecha</th>
                  <th>Valor</th>
              </thead>
              <tbody>
                  {this.state.data.map((value) => {
                      return(
                          <tr>
                              <td>{value.date}</td>
                              <td>{value.value}</td>
                          </tr>
                      )
                  })}
              </tbody>
          </Table>
          </Container>
      </Layout>
    )
  }
}

export default Detail;