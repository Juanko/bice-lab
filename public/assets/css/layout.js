import styled from 'styled-components'

export const Main = styled.main`
  width: 90%;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`;