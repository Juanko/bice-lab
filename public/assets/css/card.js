import styled from 'styled-components'
import {Link as Linker} from '@reach/router';

export const CardContainer = styled(Linker)`
  width: 30%;
  height: 150px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: lightblue;
  margin: 10px;
  flex-direction: column;
  border-radius: 25px;
  border-width: 1px;
  border-color: black;
`;