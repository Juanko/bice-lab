import styled from 'styled-components'

export const LoaderContainer = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  .overlay{
    width: 100%;
    height: 100%;
    background-color: white;
    position: absolute;
    z-index: 10;
    opacity: .7;
  }
  .container{
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: 11;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
`;