import styled from 'styled-components'
import {Link as Linker} from '@reach/router';

export const HeaderBar = styled.nav`
  width: 100%;
  height: 50px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  padding-left: 5%;
  padding-right: 5%;
  margin: 0px;
  background-color: white;
  border-bottom-color: black;
  border-bottom-width: 1px;
  div{
    height: 100%;
    width: 85%;
    text-align: right;
    ul{
      margin: 0px;
      list-style: none;
      flex-direction: row;
      height: 100%;
      display: flex;
      justify-content: flex-end;
      li{
        width: 150px;
        height: 100%;
      }
    }
  }
`;

export const LinkMenu = styled(Linker)`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  color: #005a96;
`;

export const Link = styled(Linker)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Logo = styled.img`
  width: 250px;
`;