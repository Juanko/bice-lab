import styled from 'styled-components'

export const Table = styled.table`
  width: 50%;
`;
export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;