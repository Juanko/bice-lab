FROM mhart/alpine-node

WORKDIR /usr/src/app

RUN apk add --no-cache yarn

COPY package*.json ./

RUN yarn

COPY . .

RUN yarn run build

EXPOSE 3000

CMD [ "yarn", "start" ]
