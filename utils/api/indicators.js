class Indicators {
    async getIndicators() {
        const url = `/api/indicator`;
        console.debug(url);
        const response = await fetch(url, {
            method: 'GET',
            crossDomain: true,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        })
        return response.json();
    }

    async getDetail(indicator) {
        const url = `/api/history?indicator=${indicator}`;
        console.debug(url);
        const response = await fetch(url, {
            method: 'GET',
            crossDomain: true,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
        return response.json();
    }
}

export default new Indicators();