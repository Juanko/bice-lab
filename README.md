# Bice Test App

Prueba de desarrollo de aplicación en React utilizando Nextjs para mejorar la performance

## Comenzando 🚀

Para ver el proyecto funcionando debemos clonar el repositorio en el equipo y seguir las instrucciónes de **Instalación**


### Pre-requisitos 📋

Antes de comenzar, debemos tener levantado el [proyecto con la API](https://gitlab.com/Juanko/bice-lab-api) para mostrar los datos, en caso de no encontrarse, se verá sin datos.

Además debes tener instalado [Docker](https://www.docker.com/)

### Instalación 🔧

Una vez clonado el repositorio, debes abrir una terminal en la ruta de la aplicación y ejecutar el siguiente comando:

```
docker build -t bice-lab .
```

Con este comando, crearemos la imagen de la aplicación para que pueda ejecutarse sin problemas, posterior a esto, podemos iniciar la aplicación con:

```
docker run -p 3000:3000 bice-lab 
```

Con esto, nuestra aplicación quedará levantada en la url http://localhost:3000 o la ip utilizada por Docker

PD: Puedes dejar funcionando la aplicación en background agregando **-d** al comando **docker run**

## Construido con 🛠️

Para el desarrollo se utilizó

* [ReactJS](https://es.reactjs.org/) - Biblioteca de JavaScript para interfaces
* [NextJS](https://nextjs.org/) - Framework de React para pre-renderizar elementos y mejorar el performance
